(module bst racket
  (provide make-bst size add find find-val delete size bst-tests)

  (struct bst (root size comp) #:transparent)
  (struct bst-node (key value left right) #:transparent)

  (define (make-bst comp) (bst null 0 comp))

  ; Here is a size function.  Just because I provide it doesn't mean it's complete!
  (define (size bst)
    (bst-size bst))

    (define (add tree k v)
    (match tree 
           [(bst root size comp) 
            (let-values ([(nutree added?) (add-aux root comp k v)])
              (if added? (bst nutree (+ size 1) comp)
                (bst nutree size comp)))]))

  (define (add-aux node comp k v)
    (cond [(null? node) (values (bst-node k v '() '()) #t)]
          [(equal? k (bst-node-key node)) 
           (values (bst-node k v (bst-node-left node) (bst-node-right node)) #f)]
          [(comp k (bst-node-key node)) 
           (let-values ([(subnode added?) (add-aux (bst-node-left node) comp k v)])
             (values (rebuild-left node subnode) added?))]
          [else
           (let-values ([(subnode added?) (add-aux (bst-node-right node) comp k v)])
             (values (rebuild-right node subnode) added?))] 
          ))

  ; subnode is the new tree
  (define (rebuild-left node subnode)
      (bst-node (bst-node-key node)
                (bst-node-value node)
                subnode ; rebuild the left subtree
                (bst-node-right node)))

  ; subnode is the new tree
  (define (rebuild-right node subnode)
      (bst-node (bst-node-key node)
                (bst-node-value node)
                (bst-node-left node) 
                subnode)) ; rebuild the right subtree
                 
    (define (delete bt k)
    (let ((aux (delete-aux (bst-root bt) k (bst-comp bt))))
      (bst (car aux) (- (bst-size bt) (cdr aux)) (bst-comp bt))))
  
  (define (delete-aux bnode k comp)
    (cond ((null? bnode) (cons '() 0))
          ((eq? k (bst-node-key bnode))
           (cond ((null? (bst-node-left bnode)) (cons (bst-node-right bnode) 1))
                 ((null? (bst-node-right bnode)) (cons (bst-node-left bnode) 1))
                 (else (let ((pred (findpred (bst-node-left bnode))))
                         (cons (bst-node (bst-node-key pred) (car (delete-aux (bst-node-left bnode) (bst-node-key pred) comp)) (bst-node-right bnode)) 1)))))
          ((comp k (bst-node-key bnode))
           (let ((new-left (delete-aux (bst-node-left bnode) k comp)))
             (cons (bst-node (bst-node-key bnode) (bst-node-value bnode) (car new-left) (bst-node-right bnode))
                   (cdr new-left))))
          ((comp (bst-node-key bnode) k)
           (let ((new-right (delete-aux (bst-node-right bnode) k comp)))
             (cons (bst-node (bst-node-key bnode) (bst-node-value bnode) (bst-node-left bnode) (car new-right))
                   (cdr new-right))))
          )
    )
  (define (findpred bnode)
    (if (null? (bst-node-right bnode)) bnode (findpred (bst-node-right bnode))))
  
  
  
  (define (find bst k)
    (cond [(null? (bst-root bst)) #f] ;()s error
          [else (find-aux (bst-root bst) k)]))
  
  (define (find-aux node k)
    (cond [(null? node) '()]
          [(eq? (bst-node-key node) k) (bst-node-value node)]
          [else {or (find-aux (bst-node-left node) k)
                    (find-aux (bst-node-right node) k)}]))

            
             
     
   
  (define (find-val bst v)
    (cond ((null? (bst-root bst)) #f)
          (else (findv-aux (bst-root bst) v))))
  
  (define (findv-aux node v)
    (cond ((null? node) '())
          ((eq? (bst-node-value node) v) (bst-node-key node))
          (else (or (findv-aux (bst-node-left node) v)
                    (findv-aux (bst-node-right node) v)))
          )
    )

  ; Test cases here
  (include "test-bst.rkt")
  )