
(define (substitute xx from to)
(cond ((null? xx) null)
((equal? from (car xx)) (cons to (substitute (cdr xx) from to)))
(else (cons (car xx) (substitute (cdr xx) from to)))))


(define (delete-all x xx)
(substitute xx x null))



(define (flat xx)
 (cond ((null? xx) null)
  ((list? (car xx)) (flat (append (car xx) (cdr xx))))
